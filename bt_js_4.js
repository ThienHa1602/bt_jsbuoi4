// BT1
function sapXep() {
  var a = document.getElementById("soThu1").value * 1;
  var b = document.getElementById("soThu2").value * 1;
  var c = document.getElementById("soThu3").value * 1;
  if (a < b && a < c) {
    if (b < c) {
      document.getElementById("tangdan").innerText = a + "," + b + "," + c;
    } else {
      document.getElementById("tangdan").innerText = a + "," + c + "," + b;
    }
  } else if (b < a && b < c) {
    if (a < c) {
      document.getElementById("tangdan").innerText = b + "," + a + "," + c;
    } else {
      document.getElementById("tangdan").innerText = b + "," + c + "," + a;
    }
  } else if (c < a && c < b) {
    if (a < b) {
      document.getElementById("tangdan").innerText = c + "," + a + "," + b;
    } else {
      document.getElementById("tangdan").innerText = c + "," + b + "," + a;
    }
  }
}

// BT 2
// function xinChao() {
//   var who = document.getElementById("who").value;
//   if (who == "bo") {
//     document.getElementById("xinchao").innerText = "Xin chao Bo";
//   } else if (who == "me") {
//     document.getElementById("xinchao").innerText = "Xin chao Me";
//   } else if (who == "anh trai") {
//     document.getElementById("xinchao").innerText = "Xin chao Anh trai";
//   } else if (who == "em gai") {
//     document.getElementById("xinchao").innerText = "Xin chao Em gai";
//   }
// }

function xinChao() {
  var value = document.getElementById("value").value;
  if (value == "B") {
    document.getElementById("xinchao").innerText = "Xin chao Bo";
  } else if (value == "M") {
    document.getElementById("xinchao").innerText = "Xin chao Me";
  } else if (value == "A") {
    document.getElementById("xinchao").innerText = "Xin chao Anh trai";
  } else if (value == "E") {
    document.getElementById("xinchao").innerText = "Xin chao Em gai";
  }
}

// BT 3
function chanLe() {
  var num1 = document.getElementById("num1").value * 1;
  var num2 = document.getElementById("num2").value * 1;
  var num3 = document.getElementById("num3").value * 1;
  var chan = 0;
  var le = 0;
  if (num1 % 2 == 0) {
    chan = chan + 1;
  } else {
    le = le + 1;
  }
  if (num2 % 2 == 0) {
    chan = chan + 1;
  } else {
    le = le + 1;
  }
  if (num3 % 2 == 0) {
    chan = chan + 1;
  } else {
    le = le + 1;
  }
  document.getElementById("chanLe").innerText = "Chẵn " + chan + " Lẻ " + le;
}

// BT4
function tamGiac() {
  var x = document.getElementById("canh1").value * 1;
  var y = document.getElementById("canh2").value * 1;
  var z = document.getElementById("canh3").value * 1;
  if (x + y > z && x + z > y && z + y > x) {
    if (x == y && x == z) {
      document.getElementById("tamgiac").innerText = "Tam giác đều";
    } else if (x == y || y == z || x == z) {
      document.getElementById("tamgiac").innerText = "Tam giác cân";
    } else if (
      x * x + y * y == z * z ||
      x * x + z * z == y * y ||
      z * z + y * y == x * x
    ) {
      document.getElementById("tamgiac").innerText = "Tam giác vuông";
    } else {
      document.getElementById("tamgiac").innerText = "Tam giác thường";
    }
  } else {
    document.getElementById("tamgiac").innerText = "Không phải là tam giác";
  }
}
